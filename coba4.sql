-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 28, 2024 at 08:26 AM
-- Server version: 10.4.27-MariaDB
-- PHP Version: 8.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `coba4`
--

-- --------------------------------------------------------

--
-- Table structure for table `data`
--

CREATE TABLE `data` (
  `id` int(11) UNSIGNED NOT NULL,
  `id_master_data` int(11) UNSIGNED NOT NULL,
  `kode_wilayah` varchar(7) NOT NULL,
  `nilai` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `data`
--

INSERT INTO `data` (`id`, `id_master_data`, `kode_wilayah`, `nilai`) VALUES
(1, 1, '11', 5870),
(2, 1, '12', 15311),
(3, 1, '13', 5758),
(4, 1, '14', 7899),
(5, 1, '15', 3927),
(6, 1, '16', 9000),
(7, 1, '17', 2151),
(8, 1, '18', 8825),
(9, 1, '19', 1658),
(10, 1, '21', 2502),
(11, 1, '31', 11034),
(12, 1, '32', 52786),
(13, 1, '33', 35959),
(14, 1, '34', 4065),
(15, 1, '35', 40646),
(16, 1, '36', 14249),
(17, 1, '51', 4586),
(18, 1, '52', 5376),
(19, 1, '53', 5971),
(20, 1, '61', 5433),
(21, 1, '62', 3031),
(22, 1, '63', 4578),
(23, 1, '64', 5041),
(24, 1, '65', 0),
(25, 1, '71', 2624),
(26, 1, '72', 3300),
(27, 1, '73', 9266),
(28, 1, '74', 3003),
(29, 1, '75', 1300),
(30, 1, '76', 1528),
(31, 1, '81', 1973),
(32, 1, '82', 1391),
(33, 1, '91', 1092),
(34, 1, '94', 3702),
(35, 2, '11', 819),
(36, 2, '12', 1282),
(37, 2, '13', 348),
(38, 2, '14', 491),
(39, 2, '15', 274),
(40, 2, '16', 1074),
(41, 2, '17', 302),
(42, 2, '18', 1064),
(43, 2, '19', 68),
(44, 2, '21', 128),
(45, 2, '31', 366),
(46, 2, '32', 3399),
(47, 2, '33', 3743),
(48, 2, '34', 448),
(49, 2, '35', 4112),
(50, 2, '36', 654),
(51, 2, '51', 164),
(52, 2, '52', 736),
(53, 2, '53', 1146),
(54, 2, '61', 378),
(55, 2, '62', 135),
(56, 2, '63', 192),
(57, 2, '64', 220),
(58, 2, '65', 49),
(59, 2, '71', 192),
(60, 2, '72', 410),
(61, 2, '73', 768),
(62, 2, '74', 303),
(63, 2, '75', 186),
(64, 2, '76', 151),
(65, 2, '81', 318),
(66, 2, '82', 85),
(67, 2, '91', 212),
(68, 2, '94', 926);

-- --------------------------------------------------------

--
-- Table structure for table `kode_wilayah`
--

CREATE TABLE `kode_wilayah` (
  `id` int(11) UNSIGNED NOT NULL,
  `kode_wilayah` varchar(7) NOT NULL,
  `nama_wilayah` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `kode_wilayah`
--

INSERT INTO `kode_wilayah` (`id`, `kode_wilayah`, `nama_wilayah`) VALUES
(35, '11', 'ACEH'),
(36, '12', 'SUMATERA UTARA'),
(37, '13', 'SUMATERA BARAT'),
(38, '14', 'RIAU'),
(39, '15', 'JAMBI'),
(40, '16', 'SUMATERA SELATAN'),
(41, '17', 'BENGKULU'),
(42, '18', 'LAMPUNG'),
(43, '19', 'KEPULAUAN BANGKA BELITUNG'),
(44, '21', 'KEPULAUAN RIAU'),
(45, '31', 'DKI JAKARTA'),
(46, '32', 'JAWA BARAT'),
(47, '33', 'JAWA TENGAH'),
(48, '34', 'DI YOGYAKARTA'),
(49, '35', 'JAWA TIMUR'),
(50, '36', 'BANTEN'),
(51, '51', 'BALI'),
(52, '52', 'NUSA TENGGARA BARAT'),
(53, '53', 'NUSA TENGGARA TIMUR'),
(54, '61', 'KALIMANTAN BARAT'),
(55, '62', 'KALIMANTAN TENGAH'),
(56, '63', 'KALIMANTAN SELATAN'),
(57, '64', 'KALIMANTAN TIMUR'),
(58, '65', 'KALIMANTAN UTARA'),
(59, '71', 'SULAWESI UTARA'),
(60, '72', 'SULAWESI TENGAH'),
(61, '73', 'SULAWESI SELATAN'),
(62, '74', 'SULAWESI TENGGARA'),
(63, '75', 'GORONTALO'),
(64, '76', 'SULAWESI BARAT'),
(65, '81', 'MALUKU'),
(66, '82', 'MALUKU UTARA'),
(67, '91', 'PAPUA BARAT'),
(68, '94', 'PAPUA');

-- --------------------------------------------------------

--
-- Table structure for table `master_data`
--

CREATE TABLE `master_data` (
  `id` int(11) UNSIGNED NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

--
-- Dumping data for table `master_data`
--

INSERT INTO `master_data` (`id`, `nama`) VALUES
(1, 'Jumlah Anu Penduduk'),
(2, 'Jumlah eta penduduk');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `version` varchar(255) NOT NULL,
  `class` varchar(255) NOT NULL,
  `group` varchar(255) NOT NULL,
  `namespace` varchar(255) NOT NULL,
  `time` int(11) NOT NULL,
  `batch` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_general_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `data`
--
ALTER TABLE `data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kode_wilayah`
--
ALTER TABLE `kode_wilayah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_data`
--
ALTER TABLE `master_data`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data`
--
ALTER TABLE `data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `kode_wilayah`
--
ALTER TABLE `kode_wilayah`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=69;

--
-- AUTO_INCREMENT for table `master_data`
--
ALTER TABLE `master_data`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
