<?php

use CodeIgniter\Router\RouteCollection;

/**
 * @var RouteCollection $routes
 */
// $routes->get('/', 'Home::index');

$routes->setDefaultController('Maps');
$routes->setAutoRoute(true);

$routes->get('/', 'Maps::index');
